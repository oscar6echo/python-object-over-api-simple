# Python binary data over API


## In short

Derived from the repo [Python-object-over-API](https://github.com/PierreMarion23/Python-object-over-API)

Simplified as user defined objects are forbidden.

Generalized as arbitrary nested dict are allowed.

## Notebooks

### Control function input and output

The decorator function constrains the type of args and returned value.  
Valid types are, for example:
+ Python types (`int`, `float`, `str`, `list`, `tuple`, `dict`, `datetime`)
+ numpy (`ndarray`)
+ pandas (`Timestamp`, `DataFrame`, `Series`)

For the avoidance of doubt, user defined objects are forbidden.  
If any type is invalid then the decorator raises an exception.  

Thus such functions can gradually be moved to different services and the application architecture can gradually move to micro-service, each with a tighter list of dependencies.  

See [demo_enforce_api_decorator notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/python-object-over-api-simple/raw/master/demo_enforce_api_decorator.ipynb).

### Run remote functions

Functions can be run over the api with a simple http request.  
Only the requests and pickle packages are necessary. No need for all the remote function dependencies.  
Obviously this is the main **benefit**.  
The **constraint** is that pickle/un-pickle functions must be exactly similar on each side of the API. Practically Python versions should be the same.  

See [demo_remote_client notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/python-object-over-api-simple/raw/master/demo_remote_client.ipynb).
