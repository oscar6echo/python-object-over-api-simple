
import os
import logging as logging

import flask
from flask import Flask, request
from flask_restful import Api


logging.getLogger().setLevel(logging.DEBUG)

app = Flask(__name__)
app.config['DEBUG'] = True


# create api
_api = Api(app)

# api endpoints

# Function Request
from .api.function import FunctionApi
_api.add_resource(FunctionApi, '/api/function', methods=['POST'], endpoint='function')

# Module discover
from .api.discover import DiscoverApi
_api.add_resource(DiscoverApi, '/api/discover', methods=['GET'], endpoint='discover')

logging.info('InitApp')
