
import pickle
import flask

from flask import request
from flask_restful import Resource

from importlib import import_module


class FunctionApi(Resource):
    '''
    Execute a local function with args passed through the API.
    The function arguments and retured value must be pickleable.
    '''

    def post(self):
        request_json = pickle.loads(request.data)
        print(request_json)
        module_name = request_json['module']
        function_name = request_json['function']
        function_kwargs = request_json['kwargs']

        print(module_name)
        print(function_name)
        print(function_kwargs)

        module = import_module(module_name)
        function = getattr(module, function_name)

        function_result = function(**function_kwargs)

        try:
            function_result = function(**function_kwargs)
            status_code = 200
        except:
            function_result = 'Error in {}.{}'.format(module_name, function_name)
            status_code = 521

        print(function_result)

        payload = pickle.dumps(function_result, pickle.HIGHEST_PROTOCOL)
        response = flask.make_response(payload)
        response.headers['content-type'] = 'application/octet-stream'
        response.status_code = status_code
        return response
